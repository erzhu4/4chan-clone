class RepliesController < ApplicationController

  def create
    @reply = Reply.new(body: params[:reply][:body],
                      repliable_id: params[:reply][:repliable_id],
                      repliable_type: params[:reply][:repliable_type])
    @reply.save
    redirect_to topic_url(params[:reply][:topic_id])
  end

  def destroy
  end

end
