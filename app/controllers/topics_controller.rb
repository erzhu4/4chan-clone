class TopicsController < ApplicationController

  def index
    @topics = Topic.all
    render :index
  end

  def new
    @topic = Topic.new
    render :new
  end

  def create
    @topic = Topic.new(top_params)
    if @topic.save
      redirect_to topic_url(@topic.id)
    else
      flash[:errors] = @topic.errors.full_messages
      render :new
    end
  end

  def show
    @topic = Topic.find_by(id: params[:id])
    render :show
  end

  private
  def top_params
    params.require(:topic).permit(:title, :body)
  end

end
