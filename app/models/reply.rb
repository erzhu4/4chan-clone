class Reply < ActiveRecord::Base
  validates :body, presence: true

  belongs_to(
  :repliable,
  polymorphic: true
  )

  has_many(
  :replies, as: :repliable, dependent: :destroy
  )


end
