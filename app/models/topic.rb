class Topic < ActiveRecord::Base
  validates :title, :body, presence: true

  has_many(
  :replies, as: :repliable, dependent: :destroy
  )


end
