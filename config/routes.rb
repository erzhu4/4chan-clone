Rails.application.routes.draw do

  resources :topics, only: [:new, :create, :index, :show]

  resources :replies, only: [:create]

  root 'topics#index'

end
