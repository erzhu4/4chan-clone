class CreateReplies < ActiveRecord::Migration
  def change
    create_table :replies do |t|
      t.text :body, null: false
      t.integer :repliable_id
      t.string :repliable_type

      t.timestamps
    end
    add_index :replies, :repliable_id
  end
end
